# frozen_string_literal: true

require './lib/paydesk'

describe Paydesk do
  let!(:bills_hash) { { '50': 1, '20': 3, '10': 0 } }

  it 'returns expected hash' do
    expect(described_class.new(bills_hash, 60).call).to eq('20': 3)
  end
end
