# frozen_string_literal: true

require './lib/paydesk'

describe Paydesk do
  let!(:bills_hash) { '100: 2, 20: 3, 10: 5, 5: 1, 1: 10' }

  it 'returns expected hash' do
    skip unless Paydesk::ADDITIONAL_TASK

    expect(described_class.new(bills_hash, 199).call).to eq('100: 1, 20: 3, 10: 3, 5: 1, 1: 4')
  end
end
