# frozen_string_literal: true

require './lib/paydesk'

describe Paydesk do
  let!(:bills_hash) do
    {
      '500': 1,
      '1000': 2,
      '5000': 3,
      '10000': 4,
      '20000': 5,
      '50000': 6,
      '100000': 7
    }
  end

  it 'returns expected hash' do
    skip unless Paydesk::ADDITIONAL_TASK

    expect(described_class.new(bills_hash, 296_500).call).to eq(
      '100000': 2,
      '50000': 1,
      '20000': 2,
      '5000': 1,
      '1000': 1,
      '500': 1
    )
  end
end
